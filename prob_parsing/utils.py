# Author: Alejandro Suarez Hernandez
# INLP: Probabilistic parsing
# December 2016

import nltk
import time

from IPython.display import display, clear_output, HTML


class SimpleAccuracyScorer:
    """
    This scorer just keeps track of the accuracy, counting
    favorable cases (gold tree equal to guess) and total
    number of comparisons.
    """
    def __init__(self):
        self.good = 0.0
        self.total = 0.0
    
    def score(self, gold, guess):
        self.total += 1
        self.good += 1 if gold == guess else 0
        
    def accuracy(self):
        return self.good / self.total
        

def is_nominal_phrase(tree):
    """
    Given an NLTK tree, ascertains whether it is a nominal phrase or not.
    """
    label = tree.label()
    return label == 'NP' or label.startswith('NP-')

def has_nested_chunks(tree, filter=is_nominal_phrase):
    """
    Given an NLTK tree, ascertains whether it contains nested chunks
    that accomplish a given condition (argument filter).
    """
    for child in tree:
        istree = isinstance(child, nltk.Tree)
        if istree and next(child.subtrees(filter=filter), None) is not None:
            return True
    return False

def simplify_tree(tree, chunk_tag='NP', nested=True, flatten_chunk=False, root=True):
    """
    Given an NLTK tree, this method builds and returns a simplified tree in which
    certain chunks (e.g. the nominal phrases) are maintained, while the other syntactic
    constructions are flattened. Parameters:
        - chunk_tak: chunks to maintain
        - nested: if False, only the chunks that do not have other chunks of the same type
                  nested are maintained (e.g. only nominal phrases that do not contain other
                  nominal phrases).
        - flatten_chunk: if True, the maintained chunks will be flattened inside (i.e. it will
                         not contain nested structures aside from words). For instance:
                         (NP (DET 'the') (NN 'tomb') (P (IN 'of') (NP (DET 'the') (NN 'ancients'))))
                         will be flattened to:
                         (NP (DET 'the') (NN 'tomb') (IN 'of') (DET 'the') (NN 'ancients'))
        - root: the method is recursive, and this argument is just for internal use (should not be
                set by the caller).
    """
    if tree.height() == 2:
        return tree
    if is_nominal_phrase(tree) and (nested or not has_nested_chunks(tree)):
        if flatten_chunk:
            children = tree.subtrees(filter=lambda t: t.height()==2)
            return nltk.Tree(chunk_tag, children)
        else:
            return nltk.Tree(chunk_tag, tree)
    children = list()
    for subtree in tree:
        children_ = simplify_tree(subtree,
                                  chunk_tag=chunk_tag,
                                  flatten_chunk=flatten_chunk,
                                  nested=nested,
                                  root=False)
        if isinstance(children_, nltk.Tree):
            children.append(children_)
        else:
            children += children_
    return nltk.Tree(tree.label(), children) if root else children

def pos_tree(tree):
    """
    Given a tree, it builds and return a tree in which the terminal nodes (i.e. the leaves)
    are the words' POS. This is useful for CFG and PCFG grammar based robust chunking.
    """
    if tree.height() == 2:
        return tree.label()
    pos_tree_ = nltk.Tree(tree.label(), list())
    for subtree in tree:
        pos_tree_.append(pos_tree(subtree))
    return pos_tree_

def join_word_pos(tree):
    """
    For some methods (namely the regexp chunkers from nltk.chunk.regexp) it is necessary that
    the terminal nodes consist in a tuple in which the first element is the word and the second
    element is the POS. This method converts from the standard treebank representation (unary
    productions) to this word/POS representation.
    """
    if tree.height() == 2:
        return (tree[0], tree.label())
    children = list()
    for child in tree:
        children.append(join_word_pos(child))
    return nltk.Tree(tree.label(), children)
    
def probtree2tree(tree):
    """
    Converts a NLTK Probabilistic Tree into a regular NLTK tree (otherwise
    we cannot use the __eq__ method.
    """
    if not isinstance(tree, nltk.ProbabilisticTree):
        return tree
    children = list()
    for child in tree:
        children.append(probtree2tree(child))
    return nltk.Tree(tree.label(), children)

def first_parse_wrapper(parse_fun):
    """
    Returns a wrapper around a parse funcion that return a generator instead
    of a tree in order to just return the first tree.
    """
    def first_parse(unchunked_text):
        guess = next(parse_fun(unchunked_text), None)
        if isinstance(guess, nltk.ProbabilisticTree):
            # convert to nltk.Tree in order to be able to compare
            # it with the ground truth
            guess = probtree2tree(guess)
        return guess
    return first_parse

def measure_score(parser, test, scorer):
    """
    Convenience method for calculating the goodness of
    a parser, using a given test set as a benchmark. A
    scorer has to be provided (either the SimpleAccuracyScorer
    provided in this module or the ChunkScore from NLTK,
    in case of using a Chunker.
    """
    display_progress(0)
    start = time.clock()
    for idx, gold_tree in enumerate(test):
        unchunked_text = list(gold_tree.flatten())
        try:
            guess = parser.parse(unchunked_text)
        except ValueError as e:
            # Unknown word. Cannot parse sentence
            guess = None
        if not guess:
            # We need a tree in order to calculate the scores.
            # Just build an entirely incorrect guess as a tree with empty root
            # and no children
            guess = nltk.Tree('', list())
        scorer.score(gold_tree, guess)
        display_progress(100*float(idx+1)/len(test))
    scorer.elapsed = time.clock() - start
    scorer.mean_elapsed = (time.clock() - start) / len(test)
    return scorer
        

########
# THE FOLLOWING ARE UTILITY METHODS FOR THE REPORT
########

def display_progress(progress, text=None):
    """
    Displays a progress bar for feedback purposes.
    """
    clear_output(wait=True)
    bar_html = """
    <div class="progress">
      <div class="progress-bar" role="progressbar"
      aria-valuenow="{progress}" aria-valuemin="0" aria-valuemax="100"
      style="width: {progress}%;">
        {progress:.1f}%
      </div>
    </div>
    """.format(progress=progress)
    display(HTML(bar_html))

def display_table(data,
                  row_labels=None,
                  column_labels=None,
                  corner='',
                  fmt='',
                  caption=None):
    """
    Method for pretty-printing a table of data in HTML. Parameters:
        - row_labels: None or a list of strings (one per each row of data)
        - column_labels: None or a list of strings (one per each row of data)
        - corner: In case that row_labels and column_labels are both provided,
                  this is the text that will appear inside the left-top cell
        - fmt: common format for each of the data cells
        - caption: caption that will appear below the table
    """
    
    assert row_labels is None or len(row_labels) == len(data)
    if column_labels is not None:
        for row in data:
            assert len(row) == len(column_labels)
    header = ""
    if column_labels:
        corner = "<th>{}</th>".format(corner) if row_labels else ""
        header = corner + ''.join(
            ["<th>{}</th>".format(col) for col in column_labels])
        header = "<tr>{}</tr>".format(header)
    rows_html = ""
    for idx, row in enumerate(data):
        lbl = "<th>{}</th>".format(row_labels[idx]) if row_labels else ""
        rows_html += "<tr>" + lbl + ''.join(
            ["<td>{:{fmt}}</td>".format(elem, fmt=fmt) for elem in row]) + "</tr>"
    caption_html = "<caption style=\"text-align: center;\">{}</caption>".format(caption) if caption else ""
    table_html = "<table>{header}{rows_html}{caption}</table>".format(
        header=header, rows_html=rows_html, caption=caption_html)
    display(HTML(table_html))
    
def display_single_column_table(data, **kwargs):
    """
    Shortcut for displaying single column data (1D list).
    """
    display_table([[elem] for elem in data], **kwargs)
    
def display_tree(tree, title=None, title_style="text-align: center;", header=4):
    """
    Display NLTK tree with a title
    """
    if title:
        title_html = "<h{header} style=\"{style}\">{title}</h{header}>".format(
            header=header,
            style=title_style,
            title=title)
        display(HTML(title_html))
    display(tree)
    
def display_text(text, level="info"):
    """
    Display some text as a Bootstrap alert div. Possible levels: info, success, warning and
    danger.
    """
    text_html = "<div class=\"alert alert-{level}\" role=\"alert\">{text}</div>".format(
        level=level, text=text)
    display(HTML(text_html))
    
def display_separator():
    """
    Displays HTML ruler
    """
    display(HTML("<hr>"))
    
def visual_grammar_test(parser, test):
    """
    Given a parser and a gold test set, parse the sentences of the test set and compare
    the output to the ground truth. Then the results are shown graphically. This method
    is intended to provide some graphical feedback.
    """
    for idx, gold_tree in enumerate(test):
        display_tree(gold_tree, "Test {}, ground truth".format(idx))
        
        unchunked_text = list(test[idx].flatten())

        start = time.clock()
        try:
            guess = parser.parse(unchunked_text)
        except ValueError as e:
            display_text("Test {}: parse failed: {}".format(idx, e),
                               level="danger")
            display_separator()
            continue

        elapsed = time.clock() - start
        elapsed_text = "Elapsed: {}s".format(elapsed)

        if guess:
            display_tree(guess, "Test {}, model response".format(idx))
            if guess == gold_tree:
                display_text("Correct parsing. " + elapsed_text,
                                   level="success")
            else:
                display_text("Incorrect parsing. " + elapsed_text,
                                   level="warning")
        else:
            display_text("No parse available. " + elapsed_text,
                               level="danger")

        display_separator()
    
