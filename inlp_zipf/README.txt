Zipf Law assignment
Alejandro Suárez Hernández

The code has been tested with Python 2.7. It
probably won't work with Python 3.

The assignment has been delivered as a Jupyter
notebook. To open, simply execute:

jupyter-notebook experiments_on_the_zipf_law.ipynb

Alternatively, we have provided a HTML file
with the already computed outputs for
convenience. It can be seen without
Jupyter in the browser.
